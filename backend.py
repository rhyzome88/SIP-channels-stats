#!/usr/bin/env python3.9

import time
import sqlite3 as sq
import asyncio
import panoramisk
from panoramisk import Manager
manager = Manager(loop=asyncio.get_event_loop(),
                  host='192.168.207.222',
                  port=5038,
                  username='user',
                  secret='qwerty123',
                  )


async def callback(mngr: panoramisk.Manager, msg: panoramisk.message):
    """Catch AMI Events/Actions"""
    state_sip = {"Unknown": '<font color="red">Не доступен</font>',
                 "Unreachable": '<font color="red">Не доступен</font>',
                 "Reachable": '<font color="green">Доступен</font>'
                 }
    names_sip = {"9001": "9001 user",
             "9002": "9002 user",
             "9003": "9003 user",
             "9004": "9004 user",
             "9008": "9008 user",
             "9010": "9010 user",
             "9011": "9011 user",
             "9014": "9014 user",
             "9015": "9015 user",
             "9016": "9016 user",
             "9017": "9017 user",
             "9018": "9018 user"
             }
    otdel_a = 'division 1'
    otdel_b = 'division 2'
    otdel_c = 'division 3'
    with sq.connect("/var/www/html/cgi-enabled/test.db") as con_sip:
        cur_sip = con_sip.cursor()

        action = await mngr.send_action({'Action': 'CoreShowChannels'})
        number_of_calls = len(action)
        call_list = action[1:number_of_calls - 1]
        id = []
        checklist = []
        l = []
        for element in call_list:
            caller = element.get('Channel')
            id.append(caller)
        for s in range(9000, 9030):
            s = str(s)
            checklist.append(s)
        for s in checklist:
            for p in id:
                if s in p:
                   l.append(s)
        print('l')
        print(l)
        action_sip = await mngr.send_action({'Action': 'SIPpeerstatus'})
        number_of_sip = len(action_sip)
        sip_list = action_sip[1:number_of_sip - 1]
        id_sip = {}
        checklist_sip = []
        for s_sip in range(9000, 9030):
            s_sip = str(s_sip)
            checklist_sip.append(s_sip)
        for element_sip in sip_list:
            caller_sip = element_sip.get('Peer')
            status_sip = element_sip.get('PeerStatus')
            for s_sip in checklist_sip:
                if s_sip in caller_sip and s_sip in names_sip.keys():
                    d_sip = names_sip[s_sip]
                    r_sip = state_sip[status_sip]
                    if otdel_a in d_sip:
                        id_sip.update({d_sip: r_sip})
        for element_sip in sip_list:
            caller_sip = element_sip.get('Peer')
            status_sip = element_sip.get('PeerStatus')
            for s_sip in checklist_sip:
                if s_sip in caller_sip and s_sip in names_sip.keys():
                    d_sip = names_sip[s_sip]
                    r_sip = state_sip[status_sip]
                    if otdel_b in d_sip:
                        id_sip.update({d_sip: r_sip})
        for element_sip in sip_list:
            caller_sip = element_sip.get('Peer')
            status_sip = element_sip.get('PeerStatus')
            for s_sip in checklist_sip:
                if s_sip in caller_sip and s_sip in names_sip.keys():
                    d_sip = names_sip[s_sip]
                    r_sip = state_sip[status_sip]
                    if otdel_c in d_sip:
                        id_sip.update({d_sip: r_sip})
        n = []
        for a in l:
            if a in names_sip.keys() and a not in n:
                d = names_sip[a]
                n.append(d)
        print('n')
        print(n)
        print('idsip')
        print(id_sip)
        o = '<font color="green">В разговоре</font>'
        v = '<font color="red">Не в разговоре</font>'
        cur_sip.execute("""DELETE FROM live""")
        for key in id_sip.keys():
            if key not in n:
                cur_sip.execute(
                    """INSERT INTO live (caller, statuse, state) VALUES ('{}', '{}', '{}')""".format(key,
                                                                                                     id_sip[key],
                                                                                                     v))
            else:
                for m in n:
                    if m == key:
                        cur_sip.execute(
                            """INSERT INTO live (caller, statuse, state) VALUES ('{}', '{}', '{}')""".format(key,
                                                                                                             id_sip[key],
                                                                                                             o))
                    else:
                        cur_sip.execute(
                            """INSERT INTO live (caller, statuse, state) VALUES ('{}', '{}', '{}')""".format(key,
                                                                                                             id_sip[key],
                                                                                                             v))


async def serve():
    for i in range(1, 14):
        starttime = time.time()
        await asyncio.sleep(0.1)
        time.sleep(4.0 - ((time.time() - starttime) % 4.0))


def main(mngr: panoramisk.Manager):
    mngr.register_event('*', callback=callback)
    mngr.connect()

    try:
        mngr.loop.run_until_complete(serve())
    except (SystemExit, KeyboardInterrupt):
        mngr.loop.stop()
        exit(0)


if __name__ == '__main__':
    main(manager)

