#!/usr/bin/env python3.9

import sqlite3 as sq

with sq.connect("/var/www/html/cgi-enabled/test.db") as con:
    cur = con.cursor()
    print("Content-type: text/html")
    print("")
    print("<!DOCTYPE HTML>")
    print("<html>")
    print("<header>")
    print("<script src='http://code.jquery.com/jquery-2.0.3.js'></script>")
    print("</header>")
    print("<script type='text/javascript'>")
    print("if (self.TIMR) clearTimeout (TIMR);")
    print("TIMR = setTimeout(F1, 5000);")
    print("function F1(){")
    print("$.get('test2.py?map_path=map', function(data){")
    print("$('#map').html(data);")
    print("});")
    print("if (self.TMR) clearTimeout (TMR);")
    print("TMR = setTimeout(F1, 5000);")
    print("}")
    print("</script>")
    print("<div id='map'>")
    print("<meta name='viewport' content='width=device-width, initial-scale=1'>")
    print("<link href='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3' crossorigin='anonymous'>")
    print("<script src='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js' integrity='sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p' crossorigin='anonymous'></script>")
    print("</head>")
    print("<table class='table'>")
    print("<thead>")
    print("<tr class='table-light'>")
    print("<th scope='col'>Оператор</th>")
    print("<th scope='col'>Статус</th>")
    print("<th scope='col'>Состояние</th>")
    print("</tr")
    print("</thead>")
    print("<tbody>")
    cur.execute('SELECT * FROM live')
    result = cur.fetchall()
    n = []
    for c in result:
        if c not in n:
            n.append(c)
    for b in n[0:6]:
        q = b[0]
        w = b[1]
        e = b[2]
        print("<tr class='table-primary'>")
        print("<th scope='row'>{q}</th>".format(q=q))
        print("<td>{w}</td>".format(w=w))
        print("<td>{e}</td>".format(e=e))
        print("</tr>")
    for b in n[7:]:
        if b == n[-1]:
            z = b[0]
            x = b[1]
            j = b[2]
            print("<tr class='table-info'>")
            print("<th scope='row'>{z}</th>".format(z=z))
            print("<td>{x}</td>".format(x=x))
            print("<td>{j}</td>".format(j=j))
            print("</tr>")
            print("</tbody>")
            print("</table>")
            print("</div>")
            print("</html>")
        else:
            q = b[0]
            w = b[1]
            e = b[2]
            print("<tr class='table-secondary'>")
            print("<th scope='row'>{q}</th>".format(q=q))
            print("<td>{w}</td>".format(w=w))
            print("<td>{e}</td>".format(e=e))
            print("</tr>")

